import std.stdio;
import std.conv;
import std.algorithm;
import std.format;

private enum BUFFER_SIZE = 4096;

Token[] parse(File input)
{
	Token[] tokens;
	tokens.reserve(1024);
	size_t[] loops;
	loops.reserve(32);
	char current;
	size_t amount;

	foreach (ubyte[] buffer; input.byChunk(BUFFER_SIZE))
	{
		foreach (ubyte b; buffer)
		{
			const char ch = to!char(b);

			switch (ch)
			{
			case '+', '-', '>', '<':
				if (amount > 0)
				{
					if (current == ch)
					{
						amount += 1;
					}
					else
					{
						tokens ~= Token.fromChar(current,
								amount);
						current = ch;
						amount = 1;
					}
				}
				else
				{
					current = ch;
					amount = 1;
				}
				break;
			case ',', '.', '[', ']':
				if (amount > 0)
				{
					tokens ~= Token.fromChar(current,
							amount);
					amount = 0;
				}

				if (ch == '[')
				{
					loops ~= tokens.length;
					tokens ~= new LoopStart(0);
				}
				else if (ch == ']')
				{
					if (loops.length == 0)
					{
						throw new ParserException(']');
					}

					size_t start = loops[$ - 1];
					loops = remove(loops, loops.length - 1);

					tokens[start] = new LoopStart(
							tokens.length);
					tokens ~= new LoopEnd(start);
				}
				else
				{
					tokens ~= Token.fromChar(ch, 0);
				}
				break;
			default:
				break;
			}
		}
	}

	if (loops.length > 0)
	{
		throw new ParserException('[');
	}

	if (amount > 0)
	{
		tokens ~= Token.fromChar(current, amount);
	}

	return tokens;
}

class ParserException : Exception
{
	this(char ch)
	{
		super(format("Unmatched '%c' character", ch));
	}
}

abstract class Token
{
	static Token fromChar(char ch, size_t value)
	{
		switch (ch)
		{
		case '+':
			return new CellInc(to!ubyte(value % 256));
		case '-':
			return new CellDec(to!ubyte(value % 256));
		case '>':
			return new PtrInc(to!ushort(value % 65_536));
		case '<':
			return new PtrDec(to!ushort(value % 65_536));
		case ',':
			return new Read();
		case '.':
			return new Write();
		case '[':
			return new LoopStart(value);
		case ']':
			return new LoopEnd(value);
		default:
			throw new Error(format("Invalid value of '%c' for argument ch",
					ch));

		}
	}
}

class CellInc : Token
{
	const ubyte value;

	this(ubyte _value)
	{
		value = _value;
	}
}

class CellDec : Token
{
	const ubyte value;

	this(ubyte _value)
	{
		value = _value;
	}
}

class PtrInc : Token
{
	const ushort value;

	this(ushort _value)
	{
		value = _value;
	}
}

class PtrDec : Token
{
	const ushort value;

	this(ushort _value)
	{
		value = _value;
	}
}

class Read : Token
{
}

class Write : Token
{
}

class LoopStart : Token
{
	const size_t value;

	this(size_t _value)
	{
		value = _value;
	}
}

class LoopEnd : Token
{
	const size_t value;

	this(size_t _value)
	{
		value = _value;
	}
}
