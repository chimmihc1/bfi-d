import parser;
import interpreter;
import std.stdio;
import std.exception;

void main(string[] args)
{
	if (args.length < 2) {
		writefln("Usage: %s <file>", args[0]);
		return;
	}

	string name = args[1];
	File file;
	try {
		file = File(name, "rb");
	} catch(ErrnoException e) {
		stderr.writefln("ERROR: Failed to open %s: %s", name, e.message);
		return;
	}

	Token[] tokens;
	try {
		tokens = parse(file);
	} catch(ErrnoException e) {
		stderr.writefln("ERROR: Failed to read %s: %s", name, e.message);
		return;
	} catch(ParserException e) {
		stderr.writefln("ERROR: Failed to parse %s: %s", name, e.message);
		return;
	}

	ulong cycles = run(tokens, stdin, stdout);
	writefln("\nInstructions ran: %d", cycles);
}
