import parser;
import std.stdio;

ulong run(Token[] tokens, File input, File output)
{
	ulong cycles;
	size_t i;
	const size_t len = tokens.length;

	ubyte[65_536] array;
	ushort ptr;

	while (i < len)
	{
		const Token token = tokens[i];
		if (cast(CellInc) token)
		{
			array[ptr] += (cast(CellInc) token).value;
		}
		else if (cast(CellDec) token)
		{
			array[ptr] -= (cast(CellDec) token).value;
		}
		else if (cast(PtrInc) token)
		{
			ptr += (cast(PtrInc) token).value;
		}
		else if (cast(PtrDec) token)
		{
			ptr -= (cast(PtrDec) token).value;
		}
		else if (cast(Read) token)
		{
			ubyte[1] buffer;
			if (input.rawRead(buffer).length == 0)
			{
				array[ptr] = 0;
			}
			else
			{
				array[ptr] = buffer[0];
			}
		}
		else if (cast(Write) token)
		{
			ubyte[1] buffer = [array[ptr]];
			output.rawWrite(buffer);
			output.flush();
		}
		else if (cast(LoopStart) token)
		{
			if (array[ptr] == 0)
			{
				i = (cast(LoopStart) token).value;
			}
		}
		else if (cast(LoopEnd) token)
		{
			if (array[ptr] != 0)
			{
				i = (cast(LoopEnd) token).value;
			}
		}

		i += 1;
		cycles += 1;
	}

	return cycles;
}
